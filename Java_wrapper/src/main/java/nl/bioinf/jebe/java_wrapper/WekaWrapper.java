package nl.bioinf.jebe.java_wrapper;

import nl.bioinf.jebe.cli.CommandLineArgumentReceiver;
import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.unsupervised.instance.RemovePercentage;


import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.Filter;

public class WekaWrapper {

    public static void main(String[] args) throws Exception {

        CommandLineArgumentReceiver clar = new CommandLineArgumentReceiver();
        // getting command line arguments
        clar.setClArgs(args);
        clar.argOptions();
        clar.processCommandLine();


        final String modelFile = "final_model_2.model";
        WekaWrapper wrapper = new WekaWrapper();
        RandomForest classifier = wrapper.loadClassifierModel(modelFile);
        Instances unknownInstances = wrapper.loadCsv("wrapper_test_data.csv");
        wrapper.classifyNewInstances(classifier, unknownInstances);

    }

    private void classifyNewInstances(RandomForest classifier, Instances newInstances) throws Exception {
        Instances classified = new Instances(newInstances);
        for (int i = 0; i < newInstances.numInstances(); i++){
            double numericLabel = classifier.classifyInstance(newInstances.get(i));
            String Stringlabel = "";
            if (numericLabel < 8){
                Stringlabel = "1 week.";
            }
            if (numericLabel >= 8 && numericLabel <15){
                Stringlabel = "2 weeks.";
            }
            if (numericLabel >= 15 && numericLabel < 22){
                Stringlabel = "3 weeks.";
            }
            if (numericLabel >= 21 && numericLabel <31){
                Stringlabel = "4 weeks.";
            }
            if (numericLabel == 31){
                Stringlabel = "4 - 6 weeks.";
            }
            if (numericLabel == 32){
                Stringlabel = "6 - 8 weeks.";
            }
            if (numericLabel == 33){
                Stringlabel = "2 - 3 months.";
            }
            if (numericLabel == 34){
                Stringlabel = "3 - 4 months.";
            }
            if (numericLabel == 35){
                Stringlabel = "4 - 6 months.";
            }
            if (numericLabel == 36){
                Stringlabel = "6 - 12 months.";
            }
            if (numericLabel == 37){
                Stringlabel = "over a year.";
            }
            System.out.println("Length of stay for patient " + (i+1) + " is estimated at " + Stringlabel);
        }
    }

    private RandomForest loadClassifierModel(String modelFile) throws Exception {
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    private Instances loadCsv(String file) throws IOException {
        try{
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(new File(file));
            csvLoader.setFieldSeparator(";");
            csvLoader.setMissingValue("?");
            Instances data = csvLoader.getDataSet();
            data.setClassIndex(1);
            System.out.println(data.classAttribute());
            return data;

        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException();
        }
    }

}

