package nl.bioinf.jebe.cli;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CommandLineArgumentReceiver {

    private static final String HELP = "help";

    private static final String FILE = "filelocation";

    private String[] clArgs = null;
    private Options options;
    private CommandLine commandLine;

    private String filename;


    public void argOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false,"prints a help message");
        Option fileOption = new Option("f", FILE, true, "Path to the file with patient data");

        options.addOption(helpOption);
        options.addOption(fileOption);

    }

    public void processCommandLine() throws IOException {
        CommandLineParser parser = new DefaultParser();
        try{
            this.commandLine = parser.parse(this.options, clArgs);}
        catch (ParseException e){
            e.printStackTrace();
        }

        if (helpRequested()){
            printHelp();
        }

        else{
            filename = commandLine.getOptionValue(FILE, "");
            Path path = Paths.get(filename);
            if (!path.toFile().exists()){
                throw new IOException("File does not exist.");
            }
        }
    }

    public void printValues(){
        try{
            System.out.println("Filename = " + filename);
        }
        catch (NullPointerException e){
            System.out.println("Missing value(s).");
        }
    }

    public boolean helpRequested(){
        return this.commandLine.hasOption(HELP);
    }

    public void printHelp(){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Tool #####", options);
    }

    public void setClArgs(String[] args) {
        this.clArgs = args;
    }

    public String getFile() {
        return filename;
    }
}
