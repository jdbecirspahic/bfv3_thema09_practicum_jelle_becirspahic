# README #

Usage:  
java WekaWrapper -f [csv file with patient data]

This program reads a csv file with the following information, in this order SERVICES, PSOURCE, SUB1, FREQ1. These things stand for the service a patient is going te receive, the source of referrence for their admittance, the primary substance the patient uses and the frequency at which the patient uses said substance. The program then estimates how long a patient will be in treatment.

There are two classes in this project.  
 - CommandLineArgumentReceiver: This class handles the command line argument and passes it to the WekaWrapper class.  
 - WekaWrapper: This class takes the argument from CommandLineArgumentReceiver and uses it to classify new instances (patients).
 