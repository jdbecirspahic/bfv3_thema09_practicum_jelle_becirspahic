# README #

This repository is part of the 9th course in Bio-informatics at the Hanze university Groningen. In this course I was assigned to use Weka, a machine learning tool, and java, a programming language, to create a program that can predict something. I chose to try to predict how long someone would approximately need to receive treatment for drug abuse based on the drug that patient abuses. 

This repo contains a java program that utilizes weka to predict how long a patient will be in treatment for drug abuse.  

Link to the Exploratory data analysis:  
https://bitbucket.org/jdbecirspahic/thema09_eda/src/master/  
 
Version = 1.0

# Downloading the repository
To download the repository click the three dots in the top right corner of the screen and click "download repository". After you do this, follow the instructions provided by Bitbucket.

# Dependecies
The Java program does require some dependencies. These are Apache CLI and the Weka java api. These depedencies can be found in the gradle.settings file in the Java_wrapper directory.

# Contact

name:           Jelle Becirspahic  
email:          j.d.becirspahic@st.hanze.nl  
studentnumber:  387615  
